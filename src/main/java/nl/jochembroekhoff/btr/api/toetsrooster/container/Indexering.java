package nl.jochembroekhoff.btr.api.toetsrooster.container;

import java.util.Date;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Een indexering voor een locatie.
 *
 * @author Jochem Broekhoff
 */
@AllArgsConstructor
public class Indexering implements java.io.Serializable {

    @Getter
    List<Toetsweek> toetsweken;
    @Getter
    Map<String, Integer> afdelingen;
    @Getter
    Map<String, Integer> leerlingen;
    /*
    @Getter
    Map<String, Integer> examennummers;
     */
    @Getter
    Date laatstBijgewerktOp;

    public Toetsweek getToetsweekByPad(String pad) {
        for (Toetsweek tw : toetsweken) {
            if (tw.getPad().equals(pad)) {
                return tw;
            }
        }
        return null;
    }

}
