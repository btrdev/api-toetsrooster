package nl.jochembroekhoff.btr.api.toetsrooster.deel.resultaat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import nl.jochembroekhoff.btr.api.IResultaat;
import nl.jochembroekhoff.btr.api.toetsrooster.container.Toets;
import nl.jochembroekhoff.btr.api.toetsrooster.container.Toetsweek;

/**
 * LaadToetsrosterResultaat.
 *
 * @author Jochem Broekhoff
 */
@AllArgsConstructor
public class LaadToetsroosterResultaat implements IResultaat {

    @Getter
    private final String locatieSlug;
    
    @Getter
    private final Toetsweek toetsweek;
    
    @Getter
    private final Toets[] toetsen;
    
    @Getter
    private final int examennummer;

    private boolean gelukt;

    /**
     * Verkrijg de lijst met toetsen.
     *
     * @return de array met toetsen
     */
    @Override
    public Toets[] getResultaat() {
        return toetsen;
    }

    @Override
    public boolean gelukt() {
        return gelukt;
    }

    public void setGelukt(boolean gelukt) {
        this.gelukt = gelukt;
    }

}
