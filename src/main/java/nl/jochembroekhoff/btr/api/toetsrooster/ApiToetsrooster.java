package nl.jochembroekhoff.btr.api.toetsrooster;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import nl.jochembroekhoff.btr.api.IBtrApi;
import nl.jochembroekhoff.btr.api.toetsrooster.container.Indexering;
import nl.jochembroekhoff.btr.api.toetsrooster.deel.Index;
import nl.jochembroekhoff.btr.api.toetsrooster.deel.LaadToetsrooster;
import nl.jochembroekhoff.btr.api.toetsrooster.deel.resultaat.IndexResultaat;
import nl.jochembroekhoff.btr.api.toetsrooster.deel.resultaat.LaadToetsroosterResultaat;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.http.client.utils.URIBuilder;

/**
 * Rooster API basisklasse, voor instellingen, indexering etc.
 *
 * @author Jochem Broekhoff
 * @since 1.0.0
 */
public class ApiToetsrooster implements IBtrApi {

    //Instellingen
    private static URIBuilder TOETSROOSTER_URI_BUILDER = new URIBuilder()
            .setScheme("http")
            .setHost("rooster.calvijncollege.nl")
            .setPath("/Toetsrooster/");
    public static Map<String, String> LOCATIES = new HashMap<>();
    private static final String VERSIE = "1.0.0";

    //Indexering-cache
    public static final Map<String, Indexering> INDEXERINGEN = new HashMap<>();

    //Tools
    public static final String[] MAANDEN_MAP = new String[]{"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    public static final String[] DAGEN_MAP = new String[]{"Zondag", "Maandag", "Dinsdag", "Woensdag", "Donderdag", "Vrijdag", "Zaterdag"};

    /**
     * Indexeer de locaties.
     */
    public static boolean indexeer() {
        for (String locatieSlug : LOCATIES.keySet()) {
            IndexResultaat ir = Index.indexeer(locatieSlug);
            if (ir.gelukt()) {
                Indexering index = ir.getResultaat();
                INDEXERINGEN.put(locatieSlug, index);
            } else {
                //TODO: Foutafhandeling
            }
        }
        return true;
    }

    ////////////////////////////////////////////////////////////////////////////
    //INSTELLINGEN:
    public static void setToetsroosterUrl(String url) {
        ApiToetsrooster.TOETSROOSTER_URI_BUILDER = new URIBuilder(URI.create(url));
    }

    public static String getRoosterUrl() {
        try {
            return ApiToetsrooster.TOETSROOSTER_URI_BUILDER.build().toString();
        } catch (Exception e) {
            return "";
        }
    }

    public static URIBuilder getRoosterURIBuilder() throws URISyntaxException {
        return new URIBuilder(ApiToetsrooster.TOETSROOSTER_URI_BUILDER.build());
    }

    /**
     * UserAgent.
     *
     * @return de UA string
     */
    public static String getUA() {
        return "BtrApiToetsrooster/" + ApiToetsrooster.getVersie();
    }

    public static String getVersie() {
        return ApiToetsrooster.VERSIE;
    }

    ////////////////////////////////////////////////////////////////////////////
    //TOOLS:
    public static int getMaandIndex(String in) {
        return ArrayUtils.indexOf(MAANDEN_MAP, in);
    }

    ////////////////////////////////////////////////////////////////////////////
    //MAIN:
    public static void main(String... args) {
        //Initialiseer de Api door de Rooster URL in te stellen en de locaties toe tevoegen
        ApiToetsrooster.setToetsroosterUrl("http://rooster.calvijncollege.nl/Toetsrooster/");
        ApiToetsrooster.LOCATIES.put("Goes", "Goes Klein Frankrijk");
        ApiToetsrooster.LOCATIES.put("GoesStationspark", "Goes Stationspark");
        ApiToetsrooster.LOCATIES.put("Middelburg", "Middelburg");
        ApiToetsrooster.LOCATIES.put("Tholen", "Tholen");

        //Timing
        System.out.println("Indexeren...\n\n[TIMING START]: " + new Timestamp(System.currentTimeMillis()));
        ApiToetsrooster.indexeer(); //Voer de indexeer-actie uit
        System.out.println("[TIMING   END]: " + new Timestamp(System.currentTimeMillis()));

        System.out.println("\n------------------------\n\nVul gegevens in:");
        Scanner reader = new Scanner(System.in); // Reading from System.in

        System.out.print(">> Code: ");
        String code = reader.nextLine();
        System.out.print(">> Toetsweek[TW2]: ");
        String tw = reader.nextLine();
        System.out.print(">> Type[leerlingen,afdelingen]: ");
        String type = reader.nextLine();
        System.out.print(">> Locatie-slug[Goes,GoesStationspark,Middelburg,Tholen]: ");
        String locatie = reader.nextLine();

        System.out.print(">> Json-export: ");
        String jsonUit = reader.nextLine();
        System.out.print(">> Json-export-index: ");
        String jsonUitIndex = reader.nextLine();
        System.out.print(">> Serialization-export: ");
        String serUit = reader.nextLine();

        LaadToetsroosterResultaat ltr = null;
        try {
            ltr = LaadToetsrooster.laadToetsrooster(code, tw, type, locatie);
            //ltr = LaadToetsrooster.laadToetsrooster(code, tw, "leerlingen", locatie);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Gson gson = new GsonBuilder().create();
        String json = gson.toJson(ltr);
        String json_index = gson.toJson(ApiToetsrooster.INDEXERINGEN);

        //Schrijf naar bestand
        try (PrintWriter out = new PrintWriter(jsonUit)) {
            out.println(json);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try (PrintWriter out = new PrintWriter(jsonUitIndex)) {
            out.println(json_index);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try (FileOutputStream fileOut = new FileOutputStream(serUit)) {
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(ltr);
            out.close();
        } catch (IOException i) {
            i.printStackTrace();
        }

        //Weer terug uitlezen
        LaadToetsroosterResultaat ltr_geladen = null;
        try (FileInputStream fileIn = new FileInputStream(serUit)) {
            ObjectInputStream in = new ObjectInputStream(fileIn);
            ltr_geladen = (LaadToetsroosterResultaat) in.readObject();
            in.close();
        } catch (IOException i) {
            i.printStackTrace();
            return;
        } catch (ClassNotFoundException c) {
            System.out.println("LaadRoosterRestulaat class not found");
            c.printStackTrace();
            return;
        }

        if (ltr_geladen.gelukt()) {

        }

    }
}
