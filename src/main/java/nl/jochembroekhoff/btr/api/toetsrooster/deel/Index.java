package nl.jochembroekhoff.btr.api.toetsrooster.deel;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import nl.jochembroekhoff.btr.api.toetsrooster.ApiToetsrooster;
import nl.jochembroekhoff.btr.api.toetsrooster.Parseer;
import nl.jochembroekhoff.btr.api.toetsrooster.container.Indexering;
import nl.jochembroekhoff.btr.api.toetsrooster.container.Toetsweek;
import nl.jochembroekhoff.btr.api.toetsrooster.deel.resultaat.IndexResultaat;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * Api-onderdeel voor indexering.
 *
 * @author Jochem Broekhoff
 */
public class Index {

    public static JsonParser JSON_PARSER = new JsonParser();

    /**
     * Voer een index-actie uit met de gegeven slug van een locatie.
     *
     * @param locatieSlug iets als "Goes"
     * @return het IndexResultaat met de resultaten
     */
    public static IndexResultaat indexeer(String locatieSlug) {

        Date laatstBijgewerktOp = new Date();
        final List<Toetsweek> toetsweken = new LinkedList<>();
        int afdelingenCount = 0;
        final Map<String, Integer> afdelingen = new HashMap<>();
        int leerlingenCount = 0;
        final Map<String, Integer> leerlingen = new HashMap<>();
        //int examennummersCount = 0;
        //final Map<String, Integer> examennummers = new HashMap<>();

        try {
            Connection.Response resp = Jsoup.connect(ApiToetsrooster.getRoosterUrl() + "/" + locatieSlug + "/frames/navbar.htm")
                    .method(Connection.Method.GET)
                    .userAgent(ApiToetsrooster.getUA())
                    .execute();

            Document doc = resp.parse();

            ////////////////////////////////////////////////////////////////////
            // Laatst bijgewerkt op ...
            String lboHeader = resp.header("Last-Modified");
            if (lboHeader == null) {
                //TODO: Dit is voorbeeldtekst, als er geen goede headre meegegeven wordt
                lboHeader = "Wed, 24 May 2017 09:42:13 GMT";
            }
            String[] lboTekst = lboHeader.split(" "); //Voorbeeld: Wed, 24 May 2017 09:42:13 GMT
            Calendar lboCalendar = Calendar.getInstance(TimeZone.getTimeZone(lboTekst[5]));
            //Stel de calendar samen
            lboCalendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(lboTekst[1]));
            lboCalendar.set(Calendar.MONTH, ApiToetsrooster.getMaandIndex(lboTekst[2]));
            lboCalendar.set(Calendar.YEAR, Integer.parseInt(lboTekst[3]));
            String[] lboTijd = lboTekst[4].split(":");
            lboCalendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(lboTijd[0]));
            lboCalendar.set(Calendar.MINUTE, Integer.parseInt(lboTijd[1]));
            lboCalendar.set(Calendar.SECOND, Integer.parseInt(lboTijd[2]));
            laatstBijgewerktOp = lboCalendar.getTime();

            ////////////////////////////////////////////////////////////////////
            // Toetsweken
            Element tws = doc.getElementsByAttributeValue("name", "week").first();
            for (Element tw : tws.select("option")) {
                toetsweken.add(Toetsweek.parseer(tw));
            }

            ////////////////////////////////////////////////////////////////////
            // Klassen, Docenten, Lokalen en Leerlingen
            //Haal de javascript uit navbar.htm
            String headscript = doc.select("head > script").get(0).html();

            int type = 0;
            for (String l : headscript.split("\\r?\\n")) {
                String lTrim = l.trim();
                if (lTrim.startsWith("var classes")) { //#vaag
                    //AFDELINGEN
                    type = 1;
                } else if (lTrim.startsWith("var students")) {
                    //LEERLINGEN
                    type = 2;
                } else if (lTrim.startsWith("var subjects")) { //#ookVaag 
                    //EXAMENNUMMERS
                    type = 3;
                }

                //Verwerking
                if (lTrim.length() > 0) {
                    //Als het geen lege lijn is, verwerk de inhoud
                    List<String> data = new ArrayList<>();
                    if (lTrim.contains("[") && lTrim.contains("];")) {
                        //Het is JSON, dus met GSON parseren (eenvoudiger)
                        int indexBlokhaakOp = lTrim.indexOf("[");
                        int indexBlokhaakSl = lTrim.indexOf("];");
                        String arrString = lTrim.substring(indexBlokhaakOp, indexBlokhaakSl + 1);
                        JsonArray arrJson = (JsonArray) JSON_PARSER.parse(arrString);
                        for (JsonElement arrElem : arrJson) {
                            data.add(arrElem.getAsString());
                        }
                    } else if (!lTrim.contains("[") && !lTrim.contains("];")) {
                        //Per-lijn decodering
                        int lineLen = lTrim.length();
                        String obj = lTrim;
                        if (obj.endsWith("\"")) {
                            obj = obj.substring(1, lineLen - 1);
                        } else if (obj.endsWith("\",")) {
                            obj = obj.substring(1, lineLen - 2);
                        } //else: TODO
                        data.add(obj);
                    }

                    //Herhaal de parseer-actie voor elk element
                    for (String dataElem : data) {
                        switch (type) {
                            //TODO: Afdelingen en Examennummers met VERVOLG (samenvoegen ofzo?)
                            case 1: //Afdelingen
                                afdelingenCount++;
                                afdelingen.put(Parseer.afdeling(dataElem), afdelingenCount);
                                break;
                            case 2: //Leerlingen
                                leerlingenCount++;
                                leerlingen.put(Parseer.leerlingnummer(dataElem), leerlingenCount);
                                break;
                            //case 3: //Examennummers
                            //    examennummersCount++;
                            //    examennummers.put(Parseer.afdeling(dataElem), examennummersCount);
                            //    break;
                            //Aders gewoon skippen                            
                        }
                    }
                }

                //Afsluiting
                if (lTrim.endsWith("];")) {
                    type = 0;
                }
            }
        } catch (IOException ex) {
            System.out.println("[Indexeer]: Foutmelding: " + ex.getMessage());
            //Niet nodig, als de lengte van 'namenlijst' kleiner is dan 1, wordt
            //het resultaat automatisch op niet gelukt ingesteld.
        }

        Indexering index = new Indexering(toetsweken, afdelingen, leerlingen, /*examennummers,*/ laatstBijgewerktOp);
        IndexResultaat ir = new IndexResultaat(index, true);
        return ir;
    }
}
