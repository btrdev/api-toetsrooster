package nl.jochembroekhoff.btr.api.toetsrooster.deel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import nl.jochembroekhoff.btr.api.roostering.LesTijd;
import nl.jochembroekhoff.btr.api.roostering.UurMinuut;
import nl.jochembroekhoff.btr.api.toetsrooster.ApiToetsrooster;
import nl.jochembroekhoff.btr.api.toetsrooster.Parseer;
import nl.jochembroekhoff.btr.api.toetsrooster.container.Indexering;
import nl.jochembroekhoff.btr.api.toetsrooster.container.Toets;
import nl.jochembroekhoff.btr.api.toetsrooster.container.Toetsweek;
import nl.jochembroekhoff.btr.api.toetsrooster.deel.resultaat.LaadToetsroosterResultaat;
import org.apache.http.client.utils.URIBuilder;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * LaadToetsrooster Api onderdeel.
 *
 * @author Jochem Broekhoff
 */
public class LaadToetsrooster {

    public static LaadToetsroosterResultaat laadToetsrooster(String code, String toetsweekPad, String type, String locatieSlug) throws Exception {
        //Resultaatvariabelen
        Toetsweek tw = ApiToetsrooster.INDEXERINGEN.get(locatieSlug).getToetsweekByPad(toetsweekPad);
        List<Toets> toetsen = new ArrayList<>();
        int examennummer = -1;
        boolean gelukt = true;

        //Zoek het gegeven op
        //TODO: Foutmelding geven NIET GEVONDEN als index op 0 blijft staan
        int index = -1;
        Indexering ind = ApiToetsrooster.INDEXERINGEN.get(locatieSlug);
        switch (type) {
            case "leerlingen":
                if (ind.getLeerlingen().containsKey(code)) {
                    index = ind.getLeerlingen().get(code);
                }
                break;
            case "afdelingen":
                if (ind.getAfdelingen().containsKey(code)) {
                    index = ind.getAfdelingen().get(code);
                }
                break;
        }

        if (index <= -1) {
            gelukt = false;
        } else {
            //Stel de URL samen
            String pagina = index > 1 ? "Pagina" + index : "";
            URIBuilder ub = ApiToetsrooster.getRoosterURIBuilder()
                    .setPath(ApiToetsrooster.getRoosterURIBuilder().getPath() + "/" + locatieSlug + "/" + toetsweekPad + "/" + type + "/" + type + pagina + ".html");
            String url = ub.build().toString();
            //DEBUG
            System.out.println("[URL LADEN]: " + url);
            try {
                Document doc = Jsoup.connect(url)
                        .userAgent(ApiToetsrooster.getUA())
                        .get();

                Elements eersteTables = doc.select("body > font > table");

                Calendar werkdatum = Calendar.getInstance();
                DateFormat format = new SimpleDateFormat("EEEE d MMMM yyyy", new Locale("nl_NL"));

                int tbCount = 0;
                for (Element table : eersteTables) {

                    if (tbCount < 2) {
                        //tbCount == 0 --> naam/code
                        //tbCount == 1 --> spacer
                    } else {
                        Elements tds = table.select("> tbody > tr > td");

                        //Check of het een spacing tabel is
                        Element eersteTd = tds.first();
                        if (tds.size() < 4) {
                            String txt = eersteTd.text();
                            if (eersteTd.select("> b").size() == 1) {
                                System.out.println("[DATUMAANDUIDING]: " + txt);
                                werkdatum = Parseer.datum(txt);
                            } else if (txt.trim().length() > 0) {
                                System.out.println("[NOTITIE]: " + txt);
                                if (txt.trim().equals("...")) {
                                    //TODO: Melding ofzo van er zijn geen toetsen.
                                } else if (txt.contains("examennummer ")) {
                                    int startpos = txt.indexOf("examennummer ");
                                    String exnrtxt = txt.substring(startpos + 13);
                                    System.out.println("\tEXAMENNR.: " + exnrtxt);
                                    examennummer = Integer.parseInt(exnrtxt);
                                }
                            } else {
                                System.out.println("[SPACER]");
                            }
                        } else {
                            System.out.println("\t[TOETS] op " + format.format(werkdatum.getTime()));
                            int tdCount = 0;
                            LesTijd vantot = null;
                            String vak = null;
                            String onderdeel = null;
                            String ruimte = null;
                            UurMinuut werkelijkeDuur = null;
                            int leerlingen = 0;
                            for (Element td : tds) {

                                switch (tdCount) {
                                    case 0: //VANTOT (van-tot uur)
                                        //Voorbeeld: 08:40-10:10 uur
                                        vantot = LesTijd.parseer(td.text().split(" ", 2)[0]);
                                        System.out.println("\t\tVANTOT: " + td.text() + " (" + vantot.getAantalMinuten() + " min)");
                                        break;
                                    case 1: //VAK
                                        System.out.println("\t\tVAK: " + td.text());
                                        vak = td.text();
                                        break;
                                    case 2: //ONDERDEEL
                                        System.out.println("\t\tONDERDEEL: " + td.text());
                                        onderdeel = td.text();
                                        break;
                                    case 3: //RUIMTE
                                        System.out.println("\t\tRUIMTE: " + td.text());
                                        ruimte = td.text();
                                        break;
                                    case 4: //WERKELIJKE DUUR
                                        System.out.println("\t\tWERKEL.DUUR: " + td.text());
                                        werkelijkeDuur = UurMinuut.parseer(
                                                Integer.parseInt(
                                                        td.text().split(" ", 2)[0]));
                                        break;
                                    case 5: //AANTAL LEERLINGEN (afdelingen only)
                                        if (type.equals("afdelingen")) {
                                            leerlingen = Integer.parseInt(
                                                    td.text().split(" ", 2)[0]);
                                            System.out.println("\t\tLEERLINGEN: " + leerlingen);
                                        }
                                        break;
                                }

                                tdCount++;
                            }
                            //Stel de toets samen...
                            Toets toets = new Toets(werkdatum, vantot, vak, onderdeel, ruimte, werkelijkeDuur, leerlingen);
                            //...en voeg deze toe aan de lijst
                            toetsen.add(toets);
                        }
                    }

                    tbCount++;
                }
            } catch (Exception ex) {
                //todo
                gelukt = false;
                throw ex;
            }
        }

        Toets[] toetsenArr = new Toets[toetsen.size()];
        toetsenArr = toetsen.toArray(toetsenArr);
        return new LaadToetsroosterResultaat(locatieSlug, tw, toetsenArr, examennummer, gelukt);
    }
}
