package nl.jochembroekhoff.btr.api.toetsrooster.container;

import java.util.Calendar;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import nl.jochembroekhoff.btr.api.roostering.LesTijd;
import nl.jochembroekhoff.btr.api.roostering.UurMinuut;

/**
 * Toets pojo.
 *
 * @author Jochem Broekhoff
 */
@Getter
@AllArgsConstructor
public class Toets implements java.io.Serializable {

    @NonNull
    private Calendar datum;

    @NonNull
    private LesTijd tijd;

    @NonNull
    private String vak;

    @NonNull
    private String onderdeel;

    @NonNull
    private String ruimte;

    /**
     * Werkelijke toetsduur. Deze kan verschillen van {@link #getTotaleDuur()},
     * omdat de werkelijke toetsduur meestal de totale duur minus 15 minuten is.
     * De toets wordt namelijk eerst geopend en de blaadjes worden uitgedeeld.
     */
    @NonNull
    private UurMinuut werkelijkeDuur;

    /**
     * Alleen van toepassing op toetsen weergegeven bij 'Afdelingen', omdat
     * logischerwijs in het individuele rooster niet het aantal leerlingen wordt
     * weergegeven.
     */
    @NonNull
    private Integer aantalLeerlingen;

    public int getTotaleDuur() {
        return tijd.getAantalMinuten();
    }

}
