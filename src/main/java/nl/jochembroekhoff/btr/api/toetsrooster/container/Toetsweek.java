package nl.jochembroekhoff.btr.api.toetsrooster.container;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jsoup.nodes.Element;

/**
 *
 * @author Jochem
 */
@AllArgsConstructor
public class Toetsweek implements java.io.Serializable {

    @Getter
    String pad;

    @Getter
    String beschrijving;

    @Getter
    ToetsweekType type;

    @AllArgsConstructor
    public enum ToetsweekType {
        SCHOOLEXAMEN("Schoolexamen"),
        SCHOOLEXAMEN_INHALEN("Inhalen schoolexamen"),
        CENTRAAL_EXAMEN("Centraal examen"),
        CENTRAAL_EXAMEN_INHALEN("Inhalen centraal examen"),
        REKENTOETS("Rekentoets");

        @Getter
        String omschrijving;
    }

    public static Toetsweek parseer(Element input) {
        String pad = input.val();
        String beschrijving = input.text();
        ToetsweekType type = ToetsweekType.SCHOOLEXAMEN;
        if (beschrijving.contains("TW")) {
            type = ToetsweekType.SCHOOLEXAMEN;
            if (beschrijving.contains("inhalen") || beschrijving.contains("herkansen")) {
                type = ToetsweekType.SCHOOLEXAMEN_INHALEN;
            }
        } else if (beschrijving.contains("CE")) {
            type = ToetsweekType.CENTRAAL_EXAMEN;
            if (beschrijving.contains("inhalen")) {
                type = ToetsweekType.CENTRAAL_EXAMEN_INHALEN;
            }
        } else if (beschrijving.contains("RKT")) {
            type = ToetsweekType.REKENTOETS;
        }

        return new Toetsweek(pad, beschrijving, type);
    }

}
