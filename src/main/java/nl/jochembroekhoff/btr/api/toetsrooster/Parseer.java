package nl.jochembroekhoff.btr.api.toetsrooster;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import lombok.val;
import nl.jochembroekhoff.btr.api.toetsrooster.container.Toetsweek;
import org.jsoup.nodes.Element;

/**
 * Parseer-verzamelingen.
 *
 * @author Jochem Broekhoff
 */
public class Parseer {

    private static final List<String> MAANDEN = Arrays.asList(new String[]{
        "januari", "februari", "maart", "april", "mei", "juni", "juli", "augustus", "september", "oktober", "november", "december"
    });

    public static String leerlingnummer(String input) {
        System.out.println("Parseer leerlingnummer: " + input);

        /*
        if (input.length() < 15) { //TODO: Welke waarde nemen we?
            return null;
        }

        int haakje1_index = input.indexOf("(");

        //LEERLINGNUMMER
        return input.substring(haakje1_index + 1, input.indexOf(")"));
         */
        return input.trim();
    }

    public static String afdeling(String input) {
        return input;
    }

    /**
     * Formaat zoals: maandag 18 december 2017
     *
     * @param tekst
     * @return
     */
    public static Calendar datum(String tekst) {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Europe/Amsterdam"));
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        //maandag[0] 18[1] december[2] 2017[3]
        String[] gesplit = tekst.split(" ", 4);

        int maand = MAANDEN.indexOf(gesplit[2].toLowerCase());
        if (maand < 0) {
            maand = 0;
        }

        cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(gesplit[1]));
        cal.set(Calendar.MONTH, maand);
        cal.set(Calendar.YEAR, Integer.parseInt(gesplit[3]));

        return cal;
    }

    /**
     * @deprecated gebruik Toetsweek.parseer(Element input);
     * @param input jsoup input
     * @return een toetsweek in objectformaat
     */
    public static Toetsweek toetsweek(Element input) {
        return Toetsweek.parseer(input);
    }

}
